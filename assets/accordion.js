/*
accordion on kõikide nuppude div-i class ja 
panel on kõikide sisude div -i class 
*/

let acc = document.getElementsByClassName("accordion");
let panel = document.getElementsByClassName("panel");
function activateAccordion () {
      
    // käib läbi kõik accordion classiga elemendid ja lisab clickile funktsiooni
    for (let i=0; i< acc.length; i++){
        acc[i].addEventListener("click", function openPanel(){ 
            if (acc[i].classList.contains("active")){
                acc[i].classList.toggle("active");
                let activePanel = acc[i].nextElementSibling;
                activePanel.classList.toggle("open");
            } else {
                closeAllActive();
                acc[i].classList.toggle("active");
                let activePanel = acc[i].nextElementSibling;
                activePanel.classList.toggle("open");
            }
        }       
    );
    }
    // closeAllActive();
}

function closeAllActive(){
    for (let i=0; i< panel.length; i++){
        panel[i].classList.remove("open");
    }
    for (let i=0; i< acc.length; i++){
            acc[i].classList.remove("active");
    }
}