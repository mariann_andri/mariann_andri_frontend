// funktsioonid, mis suhtlevad meie Java serveriga
//STUDIES
//kõik 
async function fetchStudies() {
    let result = await fetch(`${API_URL}/data/studies`, {
        method: 'GET',
        headers: {
            'Authorization': `Bearer ${localStorage.getItem('VALIIT_TOKEN')}`
        }
    });
    return processJsonResponse(result);
}
//üks studyID järgi - selle muuutsin ja tegin javasse uuemeetodi
async function fetchStudyById(studyId) {
    let result = await fetch(`${API_URL}/data/study/${studyId}`, {
        method: 'GET',
        headers: {
            'Authorization': `Bearer ${localStorage.getItem('VALIIT_TOKEN')}`
        }
    });
    return processJsonResponse(result);
}
//üks kliendi järgi - selle muuutsin ja tegin javasse uuemeetodi
async function fetchStudyByClient(client) {
    let result = await fetch(`${API_URL}/data/studies/${client}`, {
        method: 'GET',
        headers: {
            'Authorization': `Bearer ${localStorage.getItem('VALIIT_TOKEN')}`
        }
    });
    return processJsonResponse(result);
}

async function fetchStudiesWithShipments() {
    let result = await fetch(`${API_URL}/data/saadetised`, {
        method: 'GET',
        headers: {
            'Authorization': `Bearer ${localStorage.getItem('VALIIT_TOKEN')}`
        }
    });
    return processJsonResponse(result);
}

async function fetchStudyWithShipments(studyId) {
    let result = await fetch(`${API_URL}/data/saadetised/${studyId}`, {
        method: 'GET',
        headers: {
            'Authorization': `Bearer ${localStorage.getItem('VALIIT_TOKEN')}`
        }
    });
    return processJsonResponse(result);
}

async function fetchAllDetailsById(id){
    let result = await fetch(`${API_URL}/data/saadetised/${id}`, {
        method: 'GET',
        headers: {
            'Authorization': `Bearer ${localStorage.getItem('VALIIT_TOKEN')}`
        }
    });
    return processJsonResponse(result);
}

async function deleteStudy(studyNumber){
    let result = await fetch(`${API_URL}/data/deletestudy/${studyNumber}`,{
        method:'DELETE',
        headers: {
            'Authorization': `Bearer ${localStorage.getItem('VALIIT_TOKEN')}`
        }
    });
    return processVoidResponse(result);
}

async function deleteStudyById(id){
    let result = await fetch(`${API_URL}/data/deletestudyById/${id}`, {
        method:'DELETE',
        headers: {
            'Authorization': `Bearer ${localStorage.getItem('VALIIT_TOKEN')}`
        }
    });
    return processVoidResponse(result);
}

async function editStudies(study) {
    let result = await fetch(`${API_URL}/data/editStudy`, {
        method: 'POST',
        headers: {
            'Content-Type':'application/json',
            'Authorization': `Bearer ${localStorage.getItem('VALIIT_TOKEN')}`
        },
        body: JSON.stringify(study)
    });
    processVoidResponse(result);
}


//SHIPMENTS
async function fetchShipments() {
    let result = await fetch(`${API_URL}/data/shipments`, {
        method: 'GET',
        headers: {
            'Authorization': `Bearer ${localStorage.getItem('VALIIT_TOKEN')}`
        }
    });
    return processJsonResponse(result);
}

async function fetchShipmentsByClient(client){
    let result = await fetch(`${API_URL}/data/shipments/${client}`, {
        method: 'GET',
        headers: {
            'Authorization': `Bearer ${localStorage.getItem('VALIIT_TOKEN')}`
        }
    });
    return processJsonResponse(result);
}

async function fetchShipmentsById(id){
    let result = await fetch(`${API_URL}/data/shipment/${id}`, {
        method: 'GET',
        headers: {
            'Authorization': `Bearer ${localStorage.getItem('VALIIT_TOKEN')}`
        }
    });
    return processJsonResponse(result);
}
async function deleteShipment(id){
    let result = await fetch(`${API_URL}/data/deleteshipment/${id}`,{
        method:'DELETE',
        headers: {
            'Authorization': `Bearer ${localStorage.getItem('VALIIT_TOKEN')}`
        }  
    });
    return processVoidResponse(result);
}

async function saveShipment(shipment) {
    let result = await fetch(`${API_URL}/data/editShipment`,
    {
        method:'POST',
        headers: {
            'Content-Type':'application/json',
            'Authorization': `Bearer ${localStorage.getItem('VALIIT_TOKEN')}`
        },
        body: JSON.stringify(shipment)
    });
    return processVoidResponse(result);
}

async function fetchAllDetailsByClient(client){
    let result = await fetch(`${API_URL}/data/kliendi_saadetised/${client}`, {
        method: 'GET',
        headers: {
            'Authorization': `Bearer ${localStorage.getItem('VALIIT_TOKEN')}`
        }
    });
    return processJsonResponse(result);
}

async function countShipments(studyNumber){
    let result= await fetch(`${API_URL}/data/countshipments/${studyNumber}`, {
        method: 'GET',
        headers: {
            'Authorization': `Bearer ${localStorage.getItem('VALIIT_TOKEN')}`
        }
    })
    return processJsonResponse(result);
}


// TAP

async function deleteTap(id) {
    let result = await fetch(`${API_URL}/data/deletetap/${id}`, {
        method: 'DELETE',
        headers: {
            'Authorization': `Bearer ${localStorage.getItem('VALIIT_TOKEN')}`
        }
    })
    return processVoidResponse(result);
}

async function saveTaps(tap) {
    let result = await fetch(`${API_URL}/data/editTap`,
    {
        method:'POST',
        headers: {
            'Content-Type':'application/json',
            'Authorization': `Bearer ${localStorage.getItem('VALIIT_TOKEN')}`
        },
        body: JSON.stringify(tap)
    });
    return processVoidResponse(result);
}

async function fetchTapById(tapId) {
    let result = await fetch(`${API_URL}/data/tap/${tapId}`, {
        method: 'GET',
        headers: {
            'Authorization': `Bearer ${localStorage.getItem('VALIIT_TOKEN')}`
        }
    });
    return processJsonResponse(result);
}

async function postCredentials(credentials) {
    let result = await fetch(`${API_URL}/users/login`, {
        method: 'POST',
        headers: {
            'content-type': 'application/json'
        },
        body: JSON.stringify(credentials)
    });
    return processJsonResponse(result);
}

async function postRegistration(registration) {
    let result = await fetch(`${API_URL}/users/register`, {
        method: 'POST',
        headers: {
            'content-type': 'application/json'
        },
        body: JSON.stringify(registration)
    });
    return processJsonResponse(result);
}

async function editUserDetails(userDetails) {
    let result = await fetch(`${API_URL}/users/edit`, {
        method: 'POST',
        headers: {
            'content-type': 'application/json',
            'Authorization': `Bearer ${localStorage.getItem('VALIIT_TOKEN')}`
        },
        body: JSON.stringify(userDetails)
    });
    return processVoidResponse(result);
}


// Authentication

// Eesmärk - tuvastamise vastuse analüüsimine.

async function processJsonResponse(response) {
    // võtab sisendparameetriks response objekti
    // Nüüd analüüsime seda objekti.

    if (response.ok && response.status >= 200 && response.status < 300) {
        // Autentimine õnnestus.
        return await response.json();
    } else if (response.status >= 400 && response.status < 500) {
        // kui autentimine ebaõnnestus, siis viskame veateate.
        throw new Error('Unauthorized');
    } else {
        // midagi läks nihu, aga probleem polnud autentimises.
        throw new Error('Request failed', response);
    }
}

function processVoidResponse(response) {
        if (response.ok && response.status >= 200 && response.status < 300) {
        return;
    } else if (response.status >= 400 && response.status < 500) {
        // kui autentimine ebaõnnestus, siis viskame veateate.
        throw new Error('Unauthorized');
    } else {
        // midagi läks nihu, aga probleem polnud autentimises.
        throw new Error('Request failed', response);
    }
}