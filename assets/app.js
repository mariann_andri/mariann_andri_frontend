let currentStudyId = 0;
let currentShipmentId = 0;
let currentTapId = 0;

let studies = [];
let shipments = [];
let taps = [];


// Initialization after the HTML document has been loaded...
window.addEventListener('load', async () => {
    
    try {
        document.querySelector('#usernameSpan').textContent = localStorage.getItem('VALIIT_USERNAME');
        studies = await fetchStudiesWithShipments();
    } catch (error) {
        handleFetchError(error);
    }
    await renderStudiesWithDetails(studies);
});

/* 
    --------------------------------------------
    ACTION FUNCTIONS
    --------------------------------------------
*/

async function doSearch(event) {
    let searchWord = event.target.value.toLowerCase();
    let filteredStudies = studies.filter(
        s => s.client.toLowerCase().includes(searchWord) || s.studyNumber.toLowerCase().includes(searchWord));

    await renderStudiesWithDetails(filteredStudies);
}


//VALIDEERIMINE

function validateStudyForm() {

    let studyNumberTextBox = document.querySelector('#study-number');
    let clientTextBox = document.querySelector('#client');

    let errors =[];

    if (studyNumberTextBox.value.length < 2 || studyNumberTextBox.value.length > 50){
        errors.push('Uuringu number peab olema 2 kuni 50 tähemärki');
    }

    if (clientTextBox.value.length < 2 || clientTextBox.value.length > 100){
        errors.push('Kliendi nimi peab olema 2 kuni 100 tähemärki');
    }

    return errors;
}

function validateUserEditForm(){
    let firstPasswordInput = document.querySelector('#userEditPassword');
    let secondPasswordInput = document.querySelector('#userEditPassword2');

    let errors = [];

    if (firstPasswordInput.value != secondPasswordInput.value) {
        errors.push('Paroolid on erinevad!');
    }
    return errors;
}

//DELETE

async function doDeleteStudy(studyId) {

    // Küsime üle, kas kasutaja tõesti soovib kustutada.
    if (confirm('Soovid sa tõesti seda uuringut kustutada?')) {
        try {
            await deleteStudyById(studyId);
            studies = await fetchStudiesWithShipments();
            await renderStudiesWithDetails(studies);
        } catch (error) {
            handleStudyDeleteError(error, studyId);
        }
    }
}

async function doDeleteShipment(shipmentId) {

    let currentStudy = studies.find(s => s.shipments.some(sh => sh.id == shipmentId));

    if (confirm("Soovid sa seda saadetist kustutada?")) {
        try {
            await deleteShipment(shipmentId);
            studies = await fetchStudiesWithShipments();
            await renderStudiesWithDetails(studies);
        } catch (error) {
            handleShipmentDeleteError(error, currentStudy.id);
        }
    }
}

async function doDeleteTap(shipmentId,tapId) {
    if(confirm("Soovid sa seda tapi kustutada?"))
        await deleteTap(tapId);
        await fillTapForm(shipmentId);
}


//SAVE

async function doSaveStudy() {
    console.log('Salvestan uuringut...');

    let errors = validateStudyForm();

    if (errors.length === 0) {

        let studyNumberTextBox = document.querySelector('#study-number');
        let clientTextBox = document.querySelector('#client');

        let studyNumber = studyNumberTextBox.value;
        let client = clientTextBox.value;

        let study = {
            id: currentStudyId,
            studyNumber: studyNumber,
            client: client
        };

        console.log(study);
        await editStudies(study);
        studies = await fetchStudiesWithShipments();
        await renderStudiesWithDetails(studies);
        await closePopup();
    }

    console.log(errors);
    displayErrors(errors);
}


async function doSaveShipment() {
    // saadetise numbri(järjekorra number) võiks salvestada alles siis kui saadan välja
    console.log('Salvestan saadetist...');

    let errors = validateShipmentForm();

    let studyNumberTextBox = document.querySelector('#study-number');
    let shipmentNumberTextBox = document.querySelector('#shipment-number');
    let orderDateTextBox = document.querySelector('#order-date');
    let numberOfPatientsTextBox = document.querySelector('#number-of-patients');
    let numberOfKitsTextBox = document.querySelector('#number-of-kits');
    let expiryDateTextBox = document.querySelector('#expiry-date');
    let dispatchDateTextBox = document.querySelector('#dispatch-date');

    let studyNumber = studyNumberTextBox.value;
    let shipmentNumber = shipmentNumberTextBox.value;
    let orderDate = orderDateTextBox.value;
    let numberOfPatients = numberOfPatientsTextBox.value;
    let numberOfKits = numberOfKitsTextBox.value;
    let expiryDate = expiryDateTextBox.value;
    let dispatchDate = dispatchDateTextBox.value;

    if(expiryDate===""){
        expiryDate= null;
    }
    if(dispatchDate===""){
        dispatchDate= null;
    }

    let shipment = {
        id: currentShipmentId,
        studyNumber: studyNumber,
        shipmentNumber: shipmentNumber,
        orderDate: orderDate,
        numberOfPatients: numberOfPatients,
        numberOfKits: numberOfKits,
        expiryDate: expiryDate,
        dispatchDate: dispatchDate
    };
    console.log(shipment);
    displayErrors(errors);
    await saveShipment(shipment);
    studies = await fetchStudiesWithShipments();
    await renderStudiesWithDetails(studies);
    await closePopup();
 
}

async function doSaveTap(shipmentId) {
    console.log('Salvestan või muudan tape');

    let currentStudy = studies.find(s => s.shipments.some(sh => sh.id == shipmentId));
    let currentShipment = currentStudy.shipments.find(sh => sh.id == shipmentId);
    let currentTaps = [];

    for(let i=0; i<8; i++){
        let tapIdTextBox = document.querySelector('#tap-id'+i);
        let markingTextBox = document.querySelector('#marking'+i);
        let combinationTextBox = document.querySelector('#combination'+i);
        let tapNumberTextBox = document.querySelector('#tap-number'+i);

        let id=tapIdTextBox.value;
        let marking = markingTextBox.value;
        let combination = combinationTextBox.value;
        let tapNumber = tapNumberTextBox.value;

        if(marking !=='' || combination !=='' || tapNumber !==''){
            let tap = {
                id: id,
                shipmentId: shipmentId,
                marking: marking,
                combination: combination,
                tapNr: tapNumber
            }
            // console.log(tap);
            await saveTaps(tap);
            currentTaps.push(tap);

        }
    }
    currentShipment.taps = currentTaps;
    document.querySelector("#taps_" + currentShipment.id).innerHTML = renderTaps(currentShipment.taps);
    await closePopup();
    // loadTaps(shipmentId);
    
}

// async function loadTaps(shipmentId) {
//     let shipment = await fetchShipmentsById(shipmentId);
//     let taps = shipment.taps;
//     let tapDestination = document.querySelector('tap-details');
    
//     let tapsHtml = /*html*/`       
//     <table class= "tap-table">
//         <tr>
//             <th>Tapi number</th>
//             <th>Tapi tähis</th>
//             <th>Tapi kombinatsioon</th>
//         </tr>`;

//         for (let tap of taps) {
//             tapsHtml = tapsHtml + /*html*/`
//             <tr>
//                 <td>${tap.tapNr}</td>
//                 <td>${tap.marking}</td>
//                 <td>${tap.combination}</td>
//             </tr>
//             `;
//         }

//     tapsHtml = tapsHtml + /*html*/`
//     </table>
//     `
//     console.log(tapsHtml);
//     tapDestination.innerHTML = tapsHtml;
// }


// Login

async function doLogin() {

    let usernameInputElement = document.querySelector('#loginUserName');
    let passwordInputElement = document.querySelector('#loginPassword');

    let credentials = {
        username: usernameInputElement.value,
        password: passwordInputElement.value
    };
    doProcessLogin(credentials);    
}

// Lõime doLogini kaheks, mille teine osa on doProcessLogin().
async function doProcessLogin(credentials) {
    try {
        let userDetails = await postCredentials(credentials);
        if (userDetails.errors.length === 0){
            localStorage.setItem('VALIIT_TOKEN', userDetails.token);
            localStorage.setItem('VALIIT_USERNAME', userDetails.username);
            document.querySelector('#usernameSpan').textContent = userDetails.username;
            closePopup();
            studies = await fetchStudiesWithShipments();
            await renderStudiesWithDetails(studies);
        } else {
            displayErrors(userDetails.errors);
        }
        
    } catch (error) {
        handleLoginError(error);
    }

}

function doLogout() {
    // kustutame localstorage'ist tokenid ära.
    localStorage.removeItem('VALIIT_TOKEN');
    localStorage.removeItem('VALIIT_USERNAME');
    displayLoginPopup();
}

async function doRegister() {
    let errors = validateRegisterForm();

    displayErrors([]);
    if (errors.length === 0) {
        let usernameInputElement = document.querySelector('#registerUsername');
        let passwordInputElement = document.querySelector('#registerPassword');

        let registration = {
            username: usernameInputElement.value,
            password: passwordInputElement.value
        };
        
        let credentials = {
            username: usernameInputElement.value,
            password: passwordInputElement.value
        };

        // Logime pärast konto loomist kohe sisse.
        try {
            let registrationResult = await postRegistration(registration);
            if (registrationResult.errors.length === 0) {
                doProcessLogin(credentials);
            } else {
                displayErrors(registrationResult.errors);
            }
        } catch (error) {
            handleFetchError(error);
        }

    } else {
    displayErrors(errors);
    }
}

// doRegisteri valideerimine

function validateRegisterForm() {
    // loon tagastatava struktuuri.
    // kui kõik õnnestub:
    let errors = [];

    let usernameInputElement = document.querySelector('#registerUsername');
    let passwordInputElement = document.querySelector('#registerPassword');
    let passwordInputElement2 = document.querySelector('#registerPassword2');

    if (usernameInputElement.value.length < 2 || usernameInputElement.value.length > 190) {
        errors.push("Kasutajanimi ei vasta nõuetele (min: 2, max: 190)!");
    } 

    if (passwordInputElement.value.length < 2 || passwordInputElement.value.length > 190) {
        errors.push("Salasõna ei vasta nõuetele (min: 2, max: 190)!");
    }

    if (passwordInputElement.value !== passwordInputElement2.value) {
        errors.push("Salasõnad on erinevad!")
    }

    return errors;
}

function validateShipmentForm() {
    let errors = [];

    let orderDateInputElement = document.querySelector("#order-date");
    let expiryDateInputElement = document.querySelector("#expiry-date");
    let dispatchDateInputElement = document.querySelector("#dispatch-date");
    let numberOfPatientsInputElement = document.querySelector("#number-of-patients");
    let numberOfKitsInputElement = document.querySelector("#number-of-kits");

    let orderDate = new Date();
    orderDate = orderDateInputElement.value;

    let expiryDate = new Date();
    expiryDate = expiryDateInputElement.value;

    let dispatchDate = new Date();
    dispatchDate = dispatchDateInputElement.value;

    console.log(dispatchDate);

    if (orderDate.length === 0) {
        errors.push("Tellimusel peab olema kuupäev!");
    }

    if (expiryDate !== null && dispatchDate !== null){
        if (expiryDate < dispatchDate || expiryDate === null && dispatchDate !== null) {
            errors.push("Säilivusaeg peab olema pärast tellimuse esitamise kuupäeva!");
        }
    }
    if (orderDate !== null && dispatchDate !== null){
    if (orderDate > dispatchDate || orderDate === null && dispatchDate !== null) {
        errors.push("Väljasaatmiskuupäev ei saa olla enne tellimuse kuupäeva!")
    }
    }


    if (numberOfPatientsInputElement.value < 0) {
        errors.push("Patsientide arv ei tohi olla negatiivne");
    }

    if (numberOfKitsInputElement.value < 0) {
        errors.push("Kitide arv ei tohi olla negatiivne!");
    }

    return errors;
}  

//OPEN POPUP

async function displayLoginPopup() {
    await openPopup(POPUP_CONF_LOGIN, 'loginTemplate');

    let usernameInput = document.getElementById("loginUserName");
    let passwordInput = document.getElementById("loginPassword");

    usernameInput.addEventListener("keyup", function(event){
        if (event.keyCode === 13) {
            doLogin();
         }
    })

    passwordInput.addEventListener("keyup", function(event){
        if (event.keyCode === 13) {
            doLogin();
        }
    })
}

async function displayRegisterPopup() {
    await openPopup(POPUP_CONF_REGISTER, 'registerTemplate');

    let usernameInput = document.getElementById("registerUsername");
    let firstPasswordInput = document.getElementById("registerPassword");
    let secondPasswordInput = document.getElementById("registerPassword2");

    usernameInput.addEventListener("keyup", function(event){
        if (event.keyCode === 13) {
            doRegister();
         }
    })

    firstPasswordInput.addEventListener("keyup", function(event){
        if (event.keyCode === 13) {
            doRegister();
        }
    })

    secondPasswordInput.addEventListener("keyup", function(event){
        if (event.keyCode === 13) {
            doRegister();
        }
    })
}

async function displayStudyPopup(studyId) {
    await openPopup(POPUP_CONF_Study, 'studyEditFormTemplate');
    
    let idNumberInput = document.getElementById("#id");
    let studyNumberInput = document.getElementById("study-number");
    let studyClientInput = document.getElementById("client");

    studyNumberInput.addEventListener("keyup", function(event){
        if (event.keyCode === 13) {
            doSaveStudy();
         }
    })

    studyClientInput.addEventListener("keyup", function(event){
        if (event.keyCode === 13) {
            doSaveStudy();
        }
    })

    if (studyId > 0) {
        console.log('Muudad olemasolevat uuringut..');

        let study = await fetchStudyById(studyId);

        console.log(study);
        let idTextBox = document.querySelector('#id');
        let studyNumberTextBox = document.querySelector('#study-number');
        let clientTextBox = document.querySelector('#client');

        idTextBox.value=study.id;
        studyNumberTextBox.value = study.studyNumber;
        clientTextBox.value = study.client;
        
        currentStudyId = study.id;

    } else {
        console.log('Lisan uuringut....');
        currentStudyId = 0;
    }
    
} 

async function doOpenShipmentPopup(studyId, shipmentId) {
    await openPopup(POPUP_CONF_Shipment, 'shipmentEditFormTemplate');

    studies = await fetchStudies();
    let currentStudy = studies.find(s => s.id === studyId);
    console.log(currentStudy)

    let studyNumberTextBox = document.querySelector('#study-number');
    let shipmentNumberTextBox = document.querySelector('#shipment-number');

    if (shipmentId > 0) {
        let shipment = await fetchShipmentsById(shipmentId);
        console.log(shipment)
        console.log('Muudad olemasolevat shipmenti..');

        let orderDateTextBox = document.querySelector('#order-date');
        let numberOfPatientsTextBox = document.querySelector('#number-of-patients');
        let numberOfKitsTextBox = document.querySelector('#number-of-kits');
        let expiryDateTextBox = document.querySelector('#expiry-date');
        let dispatchDateTextBox = document.querySelector('#dispatch-date');

        studyNumberTextBox.value = shipment.studyNumber;
        shipmentNumberTextBox.value = shipment.shipmentNumber;
        orderDateTextBox.value= shipment.orderDate;
        numberOfPatientsTextBox.value = shipment.numberOfPatients;
        numberOfKitsTextBox.value = shipment.numberOfKits;
        expiryDateTextBox.value = shipment.expiryDate;
        dispatchDateTextBox.value = shipment.dispatchDate;
        currentShipmentId = shipment.id;
    } else {
        console.log('Lisan shipmenti....');
        let count=await countShipments(currentStudy.studyNumber);
        console.log("sellel uuringul on andmebaasis saadetisi " + count);
        currentShipmentId = 0;
        studyNumberTextBox.value = currentStudy.studyNumber;
        shipmentNumberTextBox.value = count+1;
    }
}

async function displayTapPopup(shipmentId) {
    await openPopup(POPUP_CONF_Tap, 'tapEditFormTemplate');
    await fillTapForm(shipmentId);

    let shipment = await fetchShipmentsById(shipmentId);
    let saveButton = document.querySelector('#save-button');
    
    saveButton.innerHTML = /*html*/`<button onclick="doSaveTap(${shipment.id})">Salvesta</button>`
    
}

async function fillTapForm(shipmentId){
    let shipment = await fetchShipmentsById(shipmentId);
    let shipmentIdTextBox = document.querySelector('#shipment-id');
    shipmentIdTextBox.value = shipmentId;
    let taps = shipment.taps;
    let tapCount= taps.length;
        for (let i=0;  i<tapCount; i++){
            let tapIdTextBox = document.querySelector('#tap-id'+i);
            let markingTextBox = document.querySelector('#marking'+i);
            let combinationTextBox = document.querySelector('#combination'+i);
            let tapNumberTextBox = document.querySelector('#tap-number'+i);
            let tapDeleteButton = document.querySelector('#tap-button'+i);

            tapIdTextBox.value = taps[i].id;
            markingTextBox.value = taps[i].marking;
            combinationTextBox.value = taps[i].combination;
            tapNumberTextBox.value = taps[i].tapNr;
            tapDeleteButton.innerHTML=/*html*/`<button onclick = "doDeleteTap(${shipmentId}, ${taps[i].id})">&#10005</button>`
        }
        for (let i=tapCount;  i<8; i++){
            
            let tapIdTextBox = document.querySelector('#tap-id'+i);
            let markingTextBox = document.querySelector('#marking'+i);
            let combinationTextBox = document.querySelector('#combination'+i);
            let tapNumberTextBox = document.querySelector('#tap-number'+i);
            let tapDeleteButton = document.querySelector('#tap-button'+i);

            tapIdTextBox.value ='';
            markingTextBox.value ='';
            combinationTextBox.value ='';
            tapNumberTextBox.value ='';
            tapDeleteButton.innerHTML='';
        }
}


async function displayUserEditPopup() {
    await openPopup(POPUP_CONF_EDIT_PASSWORD, 'userEditTemplate');

    let firstPasswordInput = document.getElementById("userEditPassword");
    let secondPasswordInput = document.getElementById("userEditPassword2");

    firstPasswordInput.addEventListener("keyup", function(event){
        if (event.keyCode === 13) {
            doEditUser();
         }
    })

    secondPasswordInput.addEventListener("keyup", function(event){
        if (event.keyCode === 13) {
            doEditUser();
        }
    })
}
 
async function doEditUser() {
    
    let errors = validateUserEditForm();

    if (errors.length === 0) {

        let userEditPassword = document.querySelector('#userEditPassword');
        let userEditPassword2 = document.querySelector('#userEditPassword2');

        if (userEditPassword.value.length >= 5 && userEditPassword.value === userEditPassword2.value) {
            try {
                let userDetails = {
                    password: userEditPassword.value
                }
                await editUserDetails(userDetails);
                closePopup();
            } catch (error) {
                handleFetchError(error);
            }
        } 
    }
    console.log(errors);
    displayErrors(errors);
}


/* 
    --------------------------------------------
    DISPLAY FUNCTIONS
    --------------------------------------------
*/

//SEARCH

//käivitab search-input väljal ENTER klahviga otsingu
// let input= document.getElementById("search-input");
// input.addEventListener("keyup", function(event){
//     if (event.keyCode === 13) {
//         searchStudyWithDetails();
//     }
// })

async function searchStudyWithDetails() {
    let clientTextBox = document.querySelector('#search-input');
    let client = clientTextBox.value;
    let studies = await fetchAllDetailsByClient(client);

    let studiesDiv = document.querySelector(".accordion-wrapper");
    let studyDetailsHtml = '';

    if (studies.length > 0){
        for(let study of studies){
            studyDetailsHtml = studyDetailsHtml + /*html*/ `
            <div class= "all-details">
                <div class= "study-details">
                    <div>Uuring: ${study.studyNumber}</div>
                    <div>Klient: ${study.client}</div>
                </div>
                <div id="shipments_${study.id}">${renderShipments(study.shipments)}</div>
                <div class= "study-buttons">
                    <button onclick="displayStudyPopup(${study.id})">Muuda</button>
                    <button onclick="doDeleteStudy(${study.id})">Kustuta</button>
                    <button onclick="doOpenShipmentPopup(${study.id}, '0')">Lisa saadetis</button>
                </div>
            </div>
            `
            }
    } else {
        studyDetailsHtml=/*html*/`Sellise kliendiga uuringut ei ole andmebaasis`
    }
    studiesDiv.innerHTML = studyDetailsHtml;
    activateAccordion();
}


function displayErrors(errors){
    let errorDivElement = document.querySelector('#errorBox')
    if(errors.length > 0){
        let errorsHtml = '';
        for(let error of errors){
            errorsHtml=errorsHtml+ /*html*/`
            <div>${error}</div>`;
            }
            errorDivElement.innerHTML= errorsHtml;
            errorDivElement.style.display='block';
        
        } else {
            errorDivElement.style.display='none';
    }
}

function displayStudyErrors(errors, studyId) {
    let errorDivElement = document.querySelector('#errorBox_' + studyId);
   
    if (errors.length > 0) {
        let errorsHtml = '';

        for (let error of errors) {
            errorsHtml = errorsHtml + /*html*/ `
            <div>${error}</div>`;
        }
        errorDivElement.innerHTML = errorsHtml;
        errorDivElement.style.display='block';
        
    } else {
        errorDivElement.style.display='none';
    }
}

function displayShipmentErrors(errors, studyId) {
    
    let errorDivElement = document.querySelector('#errorBox_' + studyId);
   
    if (errors.length > 0) {
        let errorsHtml = '';

        for (let error of errors) {
            errorsHtml = errorsHtml + /*html*/ `
            <div>${error}</div>`;
        }
        errorDivElement.innerHTML = errorsHtml;
        errorDivElement.style.display='block';
        
    } else {
        errorDivElement.style.display='none';
    }
}

function handleFetchError(error) {
    if (error.message === 'Unauthorized') {
        displayLoginPopup();
    } else {
        console.log(error);
    }
}

function handleLoginError(error) {
    if (error.message === 'Unauthorized') {
        let errors = [];
        errors.push('Kontrolli kasutajanime!');
        displayErrors(errors);
    } else {
        console.log(error);
    }
}

function handleStudyDeleteError(error, studyId) {
    if (error.message === 'Request failed') {
        let errors = [];
        errors.push('Uuringu kustutamiseks tuleb enne kustutada tema saadetised!');
        displayStudyErrors(errors, studyId);
    } else {
        console.log(error);
    }    
}

function handleShipmentDeleteError(error, studyId) {
    if (error.message === 'Request failed') {
        let errors = [];
        errors.push('Saadetise kustutamiseks tuleb enne kustutada tema tapid!');
        displayShipmentErrors(errors, studyId);
    } else {
        console.log(error);
    }    
}

async function renderStudiesWithDetails(filteredStudies) {
    let studiesDiv = document.querySelector(".accordion-wrapper");
    let studiesDetailsHtml= '';

    for(let study of filteredStudies){
        studiesDetailsHtml = studiesDetailsHtml + /*html*/ `
        <div class= "all-details">
            <div class= "study-details">
                <div class = "study-errorBox">
                    <div id="errorBox_${study.id}" style="display: none"></div>
                </div>
                <div>Uuring: ${study.studyNumber}</div>
                <div>Klient: ${study.client}</div>
            </div>
            <div>${renderShipments(study.shipments)}</div>
            <div class= "study-buttons">
                <button onclick="displayStudyPopup(${study.id})">Muuda</button>
                <button onclick="doDeleteStudy(${study.id})">Kustuta</button>
                <button onclick="doOpenShipmentPopup(${study.id}, '0')">Lisa saadetis</button>
            </div>
        </div>
        `
        }
    studiesDiv.innerHTML = studiesDetailsHtml;
    activateAccordion();
}

function renderShipments(currentShipments) {

let shipmentsHtml = '';
    for (let shipment of currentShipments) {
        let currentExpiryDate = shipment.expiryDate;
        
        if(currentExpiryDate === null){
            currentExpiryDate="sisestamata";
        }
        
        let currentDispatchDate = shipment.dispatchDate;
        
        if(currentDispatchDate === null){
            currentDispatchDate="sisestamata";
        }
        let currentDispatchInfo = "välja saadetud " + shipment.dispatchDate;
        
        if(shipment.dispatchDate === null){
            currentDispatchInfo = "välja saatmata";
        }
        
        shipmentsHtml = shipmentsHtml + /*html*/` 
            <button class="accordion">Saadetis ${shipment.shipmentNumber} (${currentDispatchInfo})</button>
            <div class="panel">
                <div class="shipment-details">
                <table id= shipment-details-table>
                <tr>
                <td>tellimuse kuupäev:</th>
                <td><b>${shipment.orderDate}</b></th>
                </tr>
                <tr>
                <td>väljasaatmise kuupäev:</th>
                <td><b>${currentDispatchDate}</b></th>
                </tr>
                <tr>
                <tr>
                <td>kõlblik kuni:</th>
                <td><b>${currentExpiryDate}</b></th>
                </tr>
                <td>patsientide arv:</th>
                <td><b>${shipment.numberOfPatients}</b></th>
                </tr>
                <tr>
                <td>kitide arv:</th>
                <td><b>${shipment.numberOfKits}</b></th>
                </tr>
                </table>
                </div>
                <div class = "tap-details" id="taps_${shipment.id}">${renderTaps(shipment.taps)}</div>
                <div class= "shipment-buttons">
                    <button onclick = "doOpenShipmentPopup('0', '${shipment.id}')">Muuda</button>
                    <button onclick = "doDeleteShipment('${shipment.id}')">Kustuta</button>
                    <button onclick = "displayTapPopup('${shipment.id}')">Tapid</button>
                </div>
                <div class = "pdf-buttons">
                    <a href='${API_URL}/files/ordersheet/${shipment.id}' target="_blank" ><button>Order sheet</button></a>
                    <a href='${API_URL}/files/dispatchnote/${shipment.id}' target="_blank" ><button>Dispatch note</button></a>
                    <a href='${API_URL}/files/pouchlabel/${shipment.id}' target="_blank" ><button>Pouch label</button></a>
                </div>
            </div>
            `;
    }
    return shipmentsHtml;
}

function renderTaps(taps) {
    let tapsHtml = /*html*/`       
    <table class= "tap-table">
    <tr>
       <th>Tapi number</th>
       <th>Tapi tähis</th>
       <th>Tapi kombinatsioon</th>
    </tr>`;
    for (let tap of taps) {
        tapsHtml = tapsHtml + /*html*/`
        <tr>
            <td>${tap.tapNr}</td>
            <td>${tap.marking}</td>
            <td>${tap.combination}</td>
        </tr>
        `;
    }
    tapsHtml=tapsHtml+ /*html*/`
    </table>
    `
    return tapsHtml;
}
